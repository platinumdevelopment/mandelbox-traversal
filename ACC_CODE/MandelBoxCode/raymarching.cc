/*
   This file is part of the Mandelbox program developed for the course
    CS/SE  Distributed Computer Systems taught by N. Nedialkov in the
    Winter of 2015-2016 at McMaster University.

    Copyright (C) 2015-2016 T. Gwosdz and N. Nedialkov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <assert.h>
#include <algorithm>
#include <stdio.h>
#include "mandelbox.h"
#include "color.h"
#include "renderer.h"
#include <accelmath.h>

//#pragma acc routine seq
//extern double DE(const vec3 &p);

//void normal (const vec3 & p, vec3 & normal, MandelBoxParams mandelBox_params);

#pragma acc routine seq
extern double MandelBoxDE(const vec3 &pos, const MandelBoxParams &mPar, double c1, double c2);

//extern MandelBoxParams mandelBox_params;

//float scale = mandelBox_params.scale;
//int num_iter = mandelBox_params.num_iter;

//#pragma acc declare create(scale)
//#pragma acc declare create(num_iter)
//#pragma acc declare create(mandelBox_params)

inline double DE(const vec3 &p, const MandelBoxParams &mandelBox_params)
{ 
  double c1 = fabs(mandelBox_params.scale - 1.0);
  double c2 = pow( fabs(mandelBox_params.scale), 1 - mandelBox_params.num_iter);
  double d = MandelBoxDE(p, mandelBox_params, c1, c2);
  return d;
}    

//#pragma acc declare create(mandelBox_params)

inline void normal(const vec3 & p, vec3 & normal, const MandelBoxParams &mandelBox_params, vec3 &e1, vec3 &e2, vec3 &e3, vec3 &normalX1, vec3 &normalX2, vec3 &normalY1, vec3 &normalY2, vec3 &normalZ1, vec3 &normalZ2)
{
  // compute the normal at p
  const double sqrt_mach_eps = 1.4901e-08;
  double magnitude;
  MAGNITUDE(magnitude,p);
  //double eps = std::max( magnitude, 1.0 )*sqrt_mach_eps;
  double eps = MAX(magnitude, 1.0)*sqrt_mach_eps;

  //vec3 e1;
  VEC(e1, eps, 0,   0);
  //vec3 e2;
  VEC(e2, 0  , eps, 0);
  //vec3 e3;
  VEC(e3, 0  , 0, eps);
 
  //vec3 normalX1, normalX2, normalY1, normalY2, normalZ1, normalZ2;
  
  ADD(normalX1, p, e1);
  SUB(normalX2, p, e1);

  ADD(normalY1, p, e2);
  SUB(normalY2, p, e2);

  ADD(normalZ1, p, e3);
  SUB(normalZ2, p, e3);

  VEC(normal, DE(normalX1, mandelBox_params) - DE(normalX2, mandelBox_params), DE(normalY1, mandelBox_params) - DE(normalY2, mandelBox_params), DE(normalZ1, mandelBox_params) - DE(normalZ2, mandelBox_params));
  
  NORMALIZE(normal);
}


#pragma acc routine seq
void rayMarch(const RenderParams &render_params, const vec3 &from, const vec3  &direction, double eps, pixelData &pix_data, const MandelBoxParams &mandelBox_params, vec3 &p, vec3 &normPos, vec3 &e1, vec3 &e2, vec3 &e3, vec3 &normalX1, vec3 &normalX2, vec3 &normalY1, vec3 &normalY2, vec3 &normalZ1, vec3 &normalZ2)
{
  double dist = 0.0;
  double totalDist = 0.0;
  
  // We will adjust the minimum distance based on the current zoom

  double epsModified = 0.0;
  
  int steps=0;
  //vec3 p;
  do 
    {      
      VEC(p, from.x + direction.x * totalDist, from.y + direction.y * totalDist, from.z + direction.z * totalDist);
      dist = DE(p, mandelBox_params);
      
      totalDist += .95*dist;
      
      epsModified = totalDist;
      epsModified*=eps;
      steps++;
    }
  while (dist > epsModified && totalDist <= render_params.maxDistance && steps < render_params.maxRaySteps);
  
  //vec3 hitNormal;
  if (dist < epsModified) 
    {
      //we didnt escape
      pix_data.escaped = false;
      
      // We hit something, or reached MaxRaySteps
      pix_data.hit = p;
      
      //figure out the normal of the surface at this point
      //vec3 normPos;
      VEC(normPos, p.x - direction.x * epsModified, p.y - direction.y * epsModified, p.z - direction.z * epsModified);
      normal(normPos, pix_data.normal, mandelBox_params, e1, e2, e3, normalX1, normalX2, normalY1, normalY2, normalZ1, normalZ2);
    }
  else 
    //we have the background colour
    pix_data.escaped = true;
}



