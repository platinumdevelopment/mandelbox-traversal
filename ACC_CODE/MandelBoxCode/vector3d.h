#ifndef vec3_h
#define vec3_h

#ifdef _OPENACC
#include <accelmath.h>
#else
#include <math.h>
#endif


typedef struct 
{
  double x, y, z;
}  vec3;

#define SET_POINT(p,v) { p.x=v[0]; p.y=v[1]; p.z=v[2]; }

#define SUBTRACT_POINT(p,v,u)			\
  {						\
  p.x=(v[0])-(u[0]);				\
  p.y=(v[1])-(u[1]);				\
  p.z=(v[2])-(u[2]);				\
}

#define NORMALIZE(p) {					\
    double fMag = ( p.x*p.x + p.y*p.y + p.z*p.z );	\
    if (fMag != 0)					\
      {							\
	double fMult = 1.0/sqrt(fMag);			\
	p.x *= fMult;					\
	p.y *= fMult;					\
	p.z *= fMult;					\
      }							\
  }


#define MAGNITUDE(m,p) 	({ m=sqrt( p.x*p.x + p.y*p.y + p.z*p.z ); })

#define DOT(d,p,n) {  d=( p.x*n.x + p.y*n.y + p.z*n.z ); }

#define MAX(a,b) ( ((a)>(b))? (a):(b))

#define VEC(v,a,b,c) { v.x = a; v.y = b; v.z = c; }

#define CLAMP(v,min,max)				\
    {							\
    const double a = v.x < min ? min : v.x;		\
    v.x = a > max ? max : a;				\
    const double b = v.y < min ? min : v.y;		\
    v.y = b > max ? max : b;				\
    const double c = v.z < min ? min : v.z;		\
    v.z = c > max ? max : c;				\
    }							\

#define ADD(v, a, b) { v.x = a.x + b.x; v.y = a.y + b.y; v.z = a.z + b.z;}

#define SUB(v, a, b) { v.x = a.x - b.x; v.y = a.y - b.y; v.z = a.z - b.z;}

#endif
