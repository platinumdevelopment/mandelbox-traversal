/*
   This file is part of the Mandelbox program developed for the course
    CS/SE  Distributed Computer Systems taught by N. Nedialkov in the
    Winter of 2015-2016 at McMaster University.

    Copyright (C) 2015-2016 T. Gwosdz and N. Nedialkov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include "color.h"
#include "mandelbox.h"
#include "camera.h"
#include "vector3d.h"
#include "3d.h"
#include <openacc.h>

//extern double getTime();
extern void   printProgress( double perc, double time );

#pragma acc routine seq
extern void UnProject(double winX, double winY, const CameraParams &camP, double* obj);

#pragma acc routine seq
extern void rayMarch (const RenderParams &render_params, const vec3 &from, const vec3  &to, double eps, pixelData &pix_data, const MandelBoxParams &mandelBox_params, vec3 &p, vec3 &normPos, vec3 &e1, vec3 &e2, vec3 &e3, vec3 &normalX1, vec3 &normalX2, vec3 &normalY1, vec3 &normalY2, vec3 &normalZ1, vec3 &normalZ2);

#pragma acc routine seq
extern void getColour(const pixelData &pixData, const RenderParams &render_params,const vec3 &from, const vec3  &direction, vec3 &sendColor);

void renderFractal(const CameraParams camera_params, 
		   const RenderParams renderer_params, 
		   unsigned char* image, const MandelBoxParams mandelBox_params)
{
  const double eps = pow(10.0, renderer_params.detail); 
  
  vec3 from;
  SET_POINT(from, camera_params.camPos);
  
  const int height = renderer_params.height;
  const int width  = renderer_params.width;
  double *farPoint = (double*)malloc(sizeof(double) *3*width*height);

  vec3 *to = (vec3*)malloc(sizeof(vec3)*width*height);
  pixelData *pix_data = (pixelData*)malloc(sizeof(pixelData)*width*height);
  vec3 *color = (vec3*)malloc(sizeof(vec3)*width*height);

  vec3 *p = (vec3*)malloc(sizeof(vec3)*width*height);

  vec3 *e1 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *e2 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *e3 = (vec3*)malloc(sizeof(vec3)*width*height);

  vec3 *normalX1 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *normalX2 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *normalY1 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *normalY2 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *normalZ1 = (vec3*)malloc(sizeof(vec3)*width*height);
  vec3 *normalZ2 = (vec3*)malloc(sizeof(vec3)*width*height);

  vec3 *normPos = (vec3*)malloc(sizeof(vec3)*width*height);

  int i,j,k,l;

//#pragma acc data copyout(image[0:3*height*width]) create(to[0:height*width],pix_data[0:height*width], i[0:height*width],color[0:height*width]) copyin(eps, from, renderer_params,mandelBox_params, camera_params)

//Edited: #pragma acc data  create(to[0:height*width],pix_data[0:height*width],
  {
  #pragma acc kernels loop independent collapse(2) copyout(image[0:3*height*width]) copyin(pix_data[0:height*width], to[0:height*width], e1[0:height*width], e2[0:height*width], e3[0:height*width], normalX1[0:height*width], normalX2[0:height*width], normalY1[0:height*width], normalY2[0:height*width], normalZ1[0:height*width], normalZ2[0:height*width], normPos[0:height*width], p[0:height*width], color[0:height*width], eps, from, renderer_params, mandelBox_params, camera_params, farPoint[:3*width*height])
  for(j = 0; j < height; j++)
    {
      //for each column pixel in the row
      //#pragma acc loop
      for(i = 0; i < width; i++)
	{
	  k = j * width + i;

	  // get point on the 'far' plane
	  // since we render one frame only, we can use the more specialized method
	  UnProject(i, j, camera_params, farPoint + 3*k);
	   
	  // to = farPoint - camera_params.camPos
          SUBTRACT_POINT(to[k], (farPoint + 3 * k), camera_params.camPos);
	  NORMALIZE(to[k]);

	  //render the pixel
	  rayMarch(renderer_params, from, to[k], eps, pix_data[k], mandelBox_params, p[k], normPos[k], e1[k], e2[k], e3[k], normalX1[k], normalX2[k], normalY1[k], normalY2[k], normalZ1[k], normalZ2[k]);

	  //get the colour at this pixel
	  //VEC(color, 1.0,1.0,1.0);
	  getColour(pix_data[k], renderer_params, from, to[k], color[k]);
      
	//color[k].x = farPoint[k*3];
	//color[k].y = farPoint[k*3+1];
	//color[k].z = farPoint[k*3+2];

	  //save colour into texture
	  l = k*3;
	  image[l+2] = (unsigned char)(color[k].x * 255);
	  image[l+1] = (unsigned char)(color[k].y * 255);
	  image[l]   = (unsigned char)(color[k].z * 255);
	}
      //printProgress((j+1)/(double)height,getTime()-time);
    }
  }
  free(to);
  free(pix_data);
  free(color);
  free(p);
  free(e1);
  free(e2);
  free(e3);
  free(normalX1);
  free(normalX2);
  free(normalY1);
  free(normalY2);
  free(normalZ1);
  free(normalZ2);
  free(normPos);
  free(farPoint);

  printf("\n rendering done:\n");
}
