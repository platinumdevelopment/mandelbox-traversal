/*
   This file is part of the Mandelbox program developed for the course
    CS/SE  Distributed Computer Systems taught by N. Nedialkov in the
    Winter of 2015-2016 at McMaster University.

    Copyright (C) 2015-2016 T. Gwosdz and N. Nedialkov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "color.h"
#include "renderer.h"
#include "vector3d.h"
#include <cmath>
#include <algorithm>

using namespace std;

//vec3 baseColor;
//vec3 backColor;

void lighting(const vec3 &n, const vec3 &color, const vec3 &pos, const vec3 &direction,  vec3 &outV)
{
  vec3 CamLight;
  double CamLightW = 1.8;// 1.27536;
  double CamLightMin = 0.3;// 0.48193;

  VEC(CamLight, 1.0, 1.0, 1.0);
  
  //vec3 nn = n -1.0;
  vec3 nn;
  nn.x = n.x - 1.0;
  nn.y = n.y - 1.0;
  nn.z = n.z - 1.0;

  //double ambient = max( CamLightMin, nn.Dot(direction) )*CamLightW;
  double dot_prod;
  DOT(dot_prod, direction, nn);
  double ambient = MAX( CamLightMin, dot_prod )*CamLightW;

  //outV = CamLight*ambient*color;
  outV.x = CamLight.x * ambient * color.x;
  outV.y = CamLight.y * ambient * color.y;
  outV.z = CamLight.z * ambient * color.z;
}

#pragma acc routine seq
void getColour(const pixelData &pixData, const RenderParams &render_params, const vec3 &from, const vec3  &direction, vec3 &hitColor)
{
  //colouring and lightning
  //vec3 baseColor;
  //VEC(baseColor,1.0,1.0,1.0);

  //vec3 backColor;
  //VEC(backColor,0.4,0.4,0.4);

  //vec3 hitColor;
  VEC(hitColor, 1.0,1.0,1.0);
  if (pixData.escaped == false) 
    {
      //apply lighting
      lighting(pixData.normal, hitColor, pixData.hit, direction, hitColor);
      
      //add normal based colouring
      if(render_params.colourType == 0 || render_params.colourType == 1)
         {
	  hitColor.x = hitColor.x * pixData.normal.x;
          hitColor.y = hitColor.y * pixData.normal.y;
          hitColor.z = hitColor.z * pixData.normal.z;

	  hitColor.x = (hitColor.x + 1.0)/2.0;
          hitColor.y = (hitColor.y + 1.0)/2.0;
          hitColor.z = (hitColor.z + 1.0)/2.0;

	  hitColor.x = hitColor.x*render_params.brightness;
          hitColor.y = hitColor.y*render_params.brightness;
          hitColor.z = hitColor.z*render_params.brightness;
	  
	  //gamma correction
	  CLAMP(hitColor, 0.0, 1.0);
	  hitColor.x = hitColor.x*hitColor.x;
          hitColor.y = hitColor.y*hitColor.y;
          hitColor.z = hitColor.z*hitColor.z;
	}
      if(render_params.colourType == 1)
	{
	  //"swap" colors
	  double t = hitColor.x;
	  hitColor.x = hitColor.z;
	  hitColor.z = t;
	}
    }
  else {
    //we have the background colour
    //hitColor = backColor;
    VEC(hitColor, 0.4, 0.4, 0.4);
  }
  
  //sendColor = hitColor;
}
